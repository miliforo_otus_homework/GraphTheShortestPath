﻿#pragma once
#include <vector>

using namespace std;
struct PathOfVertex;
struct Edge
{
    int PreviousIndex;
    int TargetIndex;
    int Weight;

    Edge(int PreviousIndex, int TargetIndex, int Weight) : PreviousIndex(PreviousIndex), TargetIndex(TargetIndex), Weight(Weight){};
    Edge(){};
};

class Node
{
public:
    vector<Edge> Children;
    bool Mark;
    char NodeName;
};

class Graph
{
public:
    vector<Node*> Nodes;
    vector<Edge> Vertexes;
    
    Node* AddNode(char NodeName);
    Node* GetNode(int Index);
    void AddVertex(int FirstIndex, int SecondIndex, int Weight);
    bool FindNodeInOathOfVertex(vector<PathOfVertex*> Vertex, int Index);

    vector<Edge> FindShortestPath(int StartNode);


};

struct PathOfVertex
{
    Node* Node; 
    int MinPath;
    void SetMaxValue() { MinPath = MaxValue; }

private:
    const int MaxValue = 10000;
};