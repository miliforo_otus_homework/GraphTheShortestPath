﻿#include "Graph.h"

#include <algorithm>
#include <iostream>
#include "map"

using namespace std;

bool SortEdgesByWeight(const Edge& First, const Edge& Second)
{
    return First.Weight < Second.Weight;
}

bool SortValuesByMinValue(const PathOfVertex& First, const PathOfVertex& Second)
{
    return First.MinPath < Second.MinPath;
}

int FindMinValue(vector<PathOfVertex*> It)
{
        int MinValue = 999999, Index = 0;

    for (int i = 0; i < It.size(); i++)
    {
        if(It[i]->MinPath < MinValue)
        {
            MinValue = It[i]->MinPath;
            Index = i;
        }
    }

    return Index; 
}

void Graph::AddVertex(int FirstIndex, int SecondIndex, int Weight)
{
    Edge EdgeRef(FirstIndex, SecondIndex, Weight);
    Vertexes.push_back(EdgeRef);
    GetNode(FirstIndex)->Children.push_back(EdgeRef);
    GetNode(SecondIndex)->Children.push_back(EdgeRef);
}

bool Graph::FindNodeInOathOfVertex(vector<PathOfVertex*> Vertex, int Index)
{
   Node* NodeRef = GetNode(Index);
    
    for (auto It : Vertex)
    {
        if (It->Node == NodeRef)
        {
            return true;
        }
    }
    return false;
}

vector<Edge> Graph::FindShortestPath(int StartNode)
{
    vector<Edge> Edges;
    vector<PathOfVertex*> Vertexes;
    vector<PathOfVertex*> VertexesData;
    
    for (auto Node : Nodes)
    {
        PathOfVertex* temp = new PathOfVertex;
        temp->Node = Node;
        temp->SetMaxValue();
        Vertexes.push_back(temp);
        VertexesData.push_back(temp);
    }

    Edges.resize(Nodes.size());
    Edges[0] = Edge(StartNode, StartNode, 0);
    
    Vertexes[StartNode]->MinPath = 0;
     while (Vertexes.size() > 0)
    {
        int MinIndex = FindMinValue(Vertexes);
        Node* CurrentNode = Vertexes[MinIndex]->Node;

        for (auto Child : CurrentNode->Children)
        {

            if(FindNodeInOathOfVertex(Vertexes, Child.TargetIndex) && FindNodeInOathOfVertex(Vertexes, Child.PreviousIndex))
            {
                int MinPath = Child.Weight + Vertexes[MinIndex]->MinPath;
                if (MinPath < VertexesData[Child.TargetIndex]->MinPath)
                {
                    VertexesData[Child.TargetIndex]->MinPath = MinPath;
                    Edges[Child.TargetIndex] = Child; 
                }
            }
        }
         
         Vertexes.erase(Vertexes.begin() + MinIndex);
    }
    
    return Edges;
}

Node* Graph::AddNode(char NodeName)
{
    auto NodeRef = new Node();
    NodeRef->NodeName = NodeName;
    Nodes.push_back(NodeRef);
    return NodeRef;
}

Node* Graph::GetNode(int Index)
{
    return Nodes.at(Index);
}