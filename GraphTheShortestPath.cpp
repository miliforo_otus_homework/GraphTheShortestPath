
#include <iostream>

#include "Graph.h"

using namespace std;

int main(int argc, char* argv[])
{
    Graph* GraphRef = new Graph;
    
    GraphRef->AddNode('A');
    GraphRef->AddNode('B');
    GraphRef->AddNode('C');
    GraphRef->AddNode('D');
    GraphRef->AddNode('F');
    GraphRef->AddNode('E');
    GraphRef->AddNode('G');

    GraphRef->AddVertex(0, 1, 2);
    GraphRef->AddVertex(0, 2, 3);
    GraphRef->AddVertex(0, 3, 6);
    GraphRef->AddVertex(1, 2, 4);
    GraphRef->AddVertex(1, 4, 9);
    GraphRef->AddVertex(2, 3, 1);
    GraphRef->AddVertex(2, 4, 7);
    GraphRef->AddVertex(2, 5, 6);
    GraphRef->AddVertex(3, 5, 4);
    GraphRef->AddVertex(4, 5, 1);
    GraphRef->AddVertex(4, 6, 5);
    GraphRef->AddVertex(5, 6, 8);

    auto Edge = GraphRef->FindShortestPath(0);

    // for (auto It : Edge)
    // {
    //     cout << It.PreviousIndex << " " << It.TargetIndex << "\n";
    // }

    return 0;
}
